import sqlite3
import sys

def connectionDB(mydb):
    connection = sqlite3.connect(mydb)

    connection.cursor().execute(
    Table_SIV = '''CREATE TABLE IF NOT EXISTS siv (
    adresse_titulaire VARCHAR(255) NOT NULL,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    immatriculation VARCHAR(255) NOT NULL,
    date_immatriculation VARCHAR(255) NOT NULL,
    vin VARCHAR(255) NOT NULL,
    marque VARCHAR(255) NOT NULL,
    denomination_commerciale VARCHAR(255) NOT NULL,
    couleur VARCHAR(255) NOT NULL,
    carrosserie VARCHAR(255) NOT NULL,
    categorie VARCHAR(255) NOT NULL,
    cylindree VARCHAR(255) NOT NULL,
    energie VARCHAR(255) NOT NULL,
    places VARCHAR(255) NOT NULL,
    poids VARCHAR(255) NOT NULL,
    puissance VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    variante VARCHAR(255) NOT NULL,
    version VARCHAR(255) NOT NULL);'''
    )

    return connection


