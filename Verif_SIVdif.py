import sqlite3

def check_immat_exists(cursor,immat):
    cursor.execute('''SELECT * FROM SIV WHERE immatriculation=''',immat)
    return cursor.fetchone()


def data_to_tuple(row,fieldnames):
    list_data = []
    [list_data.append(row[col]) for col in fieldnames]
    return tuple(list_data)


def insert(cursor,rows_data,logging):
    try:
        cursor.execute('''INSERT INTO siv VALUES()''',rows_data)
        logging.info("Insert : " + str(rows_data))
    except sqlite3.Error as identifier:
        logging.warning(identifier)


def update(cursor,immat,rows_data,logging):
    try:
        rows_data = list(rows_data)
        rows_data.append(list(immat)[0])
        cursor.execute('''UPDATE siv SET 
        adresse_titulaire=?,
        nom=?,
        prenom=?,
        immatriculation=?,
        date_immatriculation=?,
        vin=?,
        marque=?,
        denomination_commerciale=?,
        couleur=?,
        carrosserie=?,
        categorie=?,
        cylindree=?,
        energie=?,
        places=?,
        poids=?,
        puissance=?,
        type=?,
        variante=?,
        version=?
        WHERE immatriculation=?'''
        ,rows_data)
        logging.info("Update : " + str(rows_data))
    except sqlite3.Error as identifier:
        logging.warning(identifier)
