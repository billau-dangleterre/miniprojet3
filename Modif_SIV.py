from Verif_SIVdif import insert, update, data_to_tuple, check_immat_exists


def upsert_in_database(cursor,list_csv_data,fieldnames,logging):
    counter_insert = 0
    counter_update = 0
    for row in list_csv_data:
        immat = tuple([row['immatriculation']],)
        rows_data = data_to_tuple(row,fieldnames)
        # Check the immatriculation of vehicule exists in database
        immat_exists = check_immat_exists(cursor,immat)
        if immat_exists is not None and immat_exists != rows_data:
            # Update data in dataBase
            update(cursor,immat,rows_data,logging)
            counter_update +=1
        if immat_exists is None:
            # Insert data in dataBase
            insert(cursor,rows_data,logging)
            counter_insert +=1